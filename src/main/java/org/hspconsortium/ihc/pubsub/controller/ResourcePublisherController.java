package org.hspconsortium.ihc.pubsub.controller;

import ca.uhn.fhir.context.FhirContext;
import com.ihc.mercury.sm.event.Event;
import com.ihc.mercury.sm.wsclient.MercuryClient;
import org.apache.commons.lang3.StringUtils;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


@RestController()
public class ResourcePublisherController {
    private static final Logger logger = LoggerFactory.getLogger(ResourcePublisherController.class);

    @Autowired
    MercuryClient mercuryClient;
    @Autowired
    FhirContext fhirContext;

    @PostMapping(path = "/publish", consumes = "application/json", produces = "application/json")
    public @ResponseBody
    ResponseEntity<String> publishResource(HttpServletRequest request, @RequestBody String json) {
        IBaseResource resource = fhirContext.newJsonParser().parseResource(json);

        Event event = mercuryClient.createEvent();
        event.setContent(resource);
        if (StringUtils.isNotEmpty(request.getParameter("source")))
            event.setContentSource(request.getParameter("source"));
        mercuryClient.process(event);
        logger.info("Published Message to Pub/Sub with id " + event.getId());
        return new ResponseEntity<>(String.format("{processIdentification:%s}", event.getId()) , HttpStatus.OK);
    }
}

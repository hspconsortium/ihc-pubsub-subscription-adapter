package org.hspconsortium.ihc.pubsub.config;

import ca.uhn.fhir.context.FhirContext;
import com.ihc.mercury.sm.wsclient.MercuryClient;
import com.ihc.mercury.sm.wsclient.MercuryClientFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

import static com.ihc.mercury.sm.wsclient.HttpClientFactory.*;

@Configuration
public class ApplicationConfig {
    @Value("${ihc.pubsub.user}")
    private String user;

    @Value("${ihc.pubsub.password}")
    private String password;

    @Value("${ihc.pubsub.securityMode}")
    private String securityMode;

    @Value("${ihc.pubsub.serverUrl}")
    private String serverUrl;


    @Bean
    public MercuryClientFactory mercuryClientFactory() {
        if ("open".equals(securityMode)) {
            System.setProperty("com.ihc.mercury.api.SECURITY_MODE", securityMode);
        }
        Properties properties = new Properties();
        properties.setProperty(String.valueOf("com.ihc.mercury.api.SERVER_URL"), serverUrl);
        properties.setProperty(String.valueOf("com.ihc.mercury.api.CLIENT_ID"), user);
        properties.setProperty(String.valueOf("com.ihc.mercury.api.CLIENT_SECRET"), password);
//        if (System.getProperty("http.proxyHost") != null)
//            properties.setProperty(PROXY_HOST, System.getProperty("http.proxyHost"));
//        if (System.getProperty("http.proxyPort") != null)
//            properties.setProperty(PROXY_PORT, System.getProperty("http.proxyPort"));
//        if (System.getProperty("http.proxyUser") != null)
//            properties.setProperty(PROXY_AUTHENTICATION_USER, System.getProperty("http.proxyUser"));
//        if (System.getProperty("http.proxyPassword") != null)
//            properties.setProperty(PROXY_AUTHENTICATION_PASSWORD, System.getProperty("http.proxyPassword"));
        return MercuryClientFactory.getClientFactory(properties);
    }

    @Bean
    public MercuryClient mercuryClient(MercuryClientFactory mercuryClientFactory) {
        return mercuryClientFactory.getMercuryClient();
    }

    @Bean
    public FhirContext fhirContext() {
        return FhirContext.forDstu3();
    }
}

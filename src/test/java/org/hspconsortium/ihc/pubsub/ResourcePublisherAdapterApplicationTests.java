package org.hspconsortium.ihc.pubsub;

import org.apache.commons.io.IOUtils;
import org.apache.http.entity.StringEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.io.InputStream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@RunWith(SpringRunner.class)
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class ResourcePublisherAdapterApplicationTests {

	@Autowired
	private WebApplicationContext ctx;

	private String subscription;
	private String observation;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		InputStream stream = null;
		this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx).build();
		try {
			stream = ResourcePublisherAdapterApplicationTests.class.getResourceAsStream("/observation_1975-2.json");
			this.observation = IOUtils.toString(stream);
			stream = ResourcePublisherAdapterApplicationTests.class.getResourceAsStream("/subscription_1975-2.json");
			this.subscription = IOUtils.toString(stream);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(stream);
		}
	}

	@Test
	public void noParamGreetingShouldReturnDefaultMessage() throws Exception {

		this.mockMvc.perform(get("/echo"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().string("Received GET, request path: /echo"));
	}


	@Test
	public void publishObservationResource() throws Exception {

		this.mockMvc.perform(post("/publish")
				.contentType(MediaType.APPLICATION_JSON_UTF8 )
				.content(observation))
				.andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	public void publishSubscriptionResource() throws Exception {

		this.mockMvc.perform(post("/publish")
				.contentType(MediaType.APPLICATION_JSON_UTF8 )
				.content(observation))
				.andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	public void publishObservationResourceToSeeSubscripton() throws Exception {

		this.mockMvc.perform(post("/publish", observation)
				.contentType(MediaType.APPLICATION_JSON_UTF8 )
				.content(observation))
				.andDo(print())
				.andExpect(status().isOk());
	}

}
